# Self Update

## Installation

```
cd project_dir/
git submodule add 'https://gitlab.com/mareksapota-public/vcs/self-update.git' .self-update
git submodule update --init
echo 'include .self-update/make/Makefile.mk' >> Makefile
```

## Usage

### Update repo

Update repo to the latest version and sync all submodules to appropriate
versions.  Only works on the default branch, does not update submodules to
latest versions.

```
make self-update
```

#### Run a command after update

Command won't run if the repository is already up to date.  Can be used to
reinstall/rebuild the project if an updated version is available.

```
./.sideload-vcs/shell/self-update.sh -- some command
```

### Update submodules

Update all submodules to latest available versions.

```
make update-submodules
```
