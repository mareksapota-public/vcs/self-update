#!/bin/bash

# MIT Expat License

# Copyright 2020, 2021 Marek Sapota

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

set -euo pipefail

CHECKOUT_BRANCH="${CHECKOUT_BRANCH:-0}"

if [[ -z "$(git status --porcelain)" ]]; then
    CLEAN='1'
else
    CLEAN='0'
fi
if [[ "${CLEAN}" -ne 1 ]]; then
    echo 'Working directory not clean'
    exit 1
fi

DEFAULT_BRANCH="$( \
    git remote show origin | awk '/HEAD branch/ { print $3; }'
)"
BRANCH="$(git rev-parse --abbrev-ref HEAD)"
SHA="$(git rev-parse HEAD)"

if [[ "${BRANCH}" != "${DEFAULT_BRANCH}" ]]; then
    if [[ "${CHECKOUT_BRANCH}" -eq 1 ]]; then
        git checkout "${DEFAULT_BRANCH}"
    else
        echo 'Not on default branch'
        exit 1
    fi
fi

git pull --ff-only
if [[ -f '.gitmodules' ]]; then
    git submodule update --init --recursive
fi

NEW_SHA="$(git rev-parse HEAD)"

if [[ "${SHA}" != "${NEW_SHA}" ]]; then
    if [[ "${1:-}" == '--' ]]; then
        shift
        exec "${@}"
    fi
fi
